package exercises;
import java.util.Scanner;

public class Inmatning_array {

	public static void main(String[] args) {

		String namn;
		int i = 0, j = 0, antal; 
  
		Scanner in_matning = new Scanner(System.in); 

		System.out.print("Ange hur många namn/mobilnr som ska matas in:");
		antal = in_matning.nextInt();
		in_matning.nextLine(); // rensar raden så att den blir blank//
		String[] f_namn = new String[antal]; // deklarerar arrayen utifrån
												// inmatade storleken(=antal) //
		String[] e_namn = new String[antal];
		String[] m_nr = new String[antal];

		while (i < antal) { // inmatningsloopen //
			System.out.print("Förnamn: (stop för att avsluta) ");
			namn = in_matning.nextLine();
			if (!namn.equals("stop")) { // om namn är lika med stop gå till else och break//
				f_namn[i] = namn;

				System.out.print("Efternamn: ");
				namn = in_matning.nextLine();
				e_namn[i] = namn;

				System.out.print("Mobilnr: ");
				namn = in_matning.nextLine();
				m_nr[i] = namn;
				i++;
			} else {
				break; // stoppar while-loopen och går till utskrift loopen//
			}
		}
		while (j < i) { // utskriftsloopen//
			System.out.println(f_namn[j] + " " + e_namn[j] + " " + m_nr[j]);
			j++;
		}
	}
}
